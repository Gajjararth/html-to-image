const puppeteer = require("puppeteer");

const htmlToImage = async ({ htmlString, height, width }) => {

    if (!Boolean(htmlString)) {
        return { success: false, msg: "No htmlString found..." }
    }

    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.setContent(decodeURIComponent(htmlString));

    await page.setViewport({
        height: height || 1800,
        width: width || 800,
    });

    let img = await page.screenshot();
    img = img.toString("base64");

    await browser.close();

    return { img };
};

module.exports = {
    htmlToImage
};