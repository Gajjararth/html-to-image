const express = require("express");
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');

const routes = require("./routes");

const server = express();

server.use(cors());
server.use(helmet());

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: false }));

server.use("", routes());

const PORT = process.env.PORT || 3000;

server.listen(PORT, err => {
    if (err) console.log(err);
    else console.info('Server', `http://localhost:${PORT}`);
});