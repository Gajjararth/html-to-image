const { htmlToImage } = require("../../puppeteer");

const html_to_image = async (req, res) => {
    const { htmlString, height, width } = req.body;

    const response = await htmlToImage({ htmlString, height, width });

    return res.json({ success: true, ...response });
};

module.exports = {
    html_to_image
};