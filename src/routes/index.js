const { Router } = require('express');
const { html_to_image } = require('./html_to_image');

module.exports = () => {
    const route = Router();

    route.get("/", (req, res) => {
        return res.sendStatus(200);
    });

    route.post("/html_to_image", html_to_image);

    return route;
}