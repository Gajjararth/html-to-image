# HTML TO IMAGE
Generates Image out of HTML

## Development Guide

#### NOTE : Please make sure you have yarn :: [Installing yarn](https://yarnpkg.com/en/docs/install)

* Initial setup

```bash
$ npm install
```

* To watch for changes

```bash
$ npm dev
```

* To start the server

```bash
$ npm start
```

* To start the server in production

```bash
$ npm prod
```